from machine import Pin, PWM, lightsleep
from time import sleep

PWM_FREQUENCY = 1000


def pulse_width_duty_intensity(intensity):
    """
    Intensity by which the lights should shine.
    :param intensity: Float ranging from 0 (off) to 1 (full brightness)
    """

    full = 2**16-1
    return int(full * intensity)


white = PWM(Pin(27, Pin.OUT), freq=PWM_FREQUENCY, duty_u16=pulse_width_duty_intensity(1.0))
red = PWM(Pin(13, Pin.OUT), freq=PWM_FREQUENCY, duty_u16=pulse_width_duty_intensity(0.0))
green = PWM(Pin(12, Pin.OUT), freq=PWM_FREQUENCY, duty_u16=pulse_width_duty_intensity(0.0))
blue = PWM(Pin(14, Pin.OUT), freq=PWM_FREQUENCY, duty_u16=pulse_width_duty_intensity(0.0))

intensity = 0.0
step = 1 / PWM_FREQUENCY
speed = e  # Hz
while True:
    red.duty_u16(pulse_width_duty_intensity(intensity))
    sleep(abs(step) / speed)
    intensity += step
    if intensity >= 1.0:
        step *= -1
        intensity = 1.0
    elif intensity <= 0.0:
        step *= -1
        intensity = 0.0
