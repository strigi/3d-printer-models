union() {
    cylinder(d=114, h=3, $fn=300);
    translate([0, 0, 7.5])
        sphere(d=15, $fn=100);    
}