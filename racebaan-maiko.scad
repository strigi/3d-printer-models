plateWidth = 41;
plateHeight = 87;
plateDepth = 1.6;

barWidth = 2;
barDepth = 1.5;
barLeftRightPadding = 3.6;
barTopBottomPadding = 10;

frictionerHeight = 0.2;
frictionerWidth = 0.4;

barLength = plateHeight - 2 * barTopBottomPadding;

plate();
bulges();
bars();
frictioners(20);

module plate() {
    cutSlack = 1;
    
    plateHoleHeight = barLength / 2 - barTopBottomPadding;
    plateHoleWidth = plateWidth - barTopBottomPadding * 2;
    
    difference() {
        cube([plateWidth, plateHeight, plateDepth]);    
        translate([barTopBottomPadding, 0, 0]) holeCube();
        translate([barTopBottomPadding, barLength - plateHoleHeight , 0]) holeCube();
        sideCuts();
    }
    
    module holeCube() {        
        translate([0, barTopBottomPadding, -cutSlack / 2])
        cube([plateHoleWidth, plateHoleHeight, plateDepth + cutSlack]);
    }
    
    module sideCuts() {
        sideCut(plateWidth, 0);
        sideCut(0, 0);
        sideCut(0, plateHeight);
        sideCut(plateWidth, plateHeight);
    }
    
    module sideCut(x, y) {
        sideCutSide = 5;
        translate([x, y-sqrt(2)*sideCutSide/2, -cutSlack / 2])
        rotate([0, 0, 45])
        cube([sideCutSide, sideCutSide, plateDepth + cutSlack]);
    }
}

module bulges() {
    countPerSide = 4;
    bulgeThickness = 0.5;
    radius = 5;
    
    dis = (barLength - radius*2) / (countPerSide - 1);

    side(-bulgeThickness);
    side(plateWidth - radius * 2 + bulgeThickness);
    
    module side(xOffset) {
        for(i = [0 : countPerSide-1]) {
            translate([xOffset + radius, barTopBottomPadding + radius + i * dis])
            cylinder($fn=50, r=radius, h=plateDepth);
        }
    }        
}

module bars() {       
    translate([barLeftRightPadding, barTopBottomPadding, plateDepth])
    cube([barWidth, barLength, barDepth]);
    
    translate([plateWidth - barWidth - barLeftRightPadding, barTopBottomPadding, plateDepth])
    cube([barWidth, barLength, barDepth]);
}

module frictioners(count) {
    lim = barLength - frictionerWidth;
    dis = lim / (count - 1);
    
    strip(0);
    strip(plateWidth - barLeftRightPadding);
    
    module strip(xOffset) {
        for(i = [0 : count - 1]) {
            translate([xOffset, barTopBottomPadding + (i * dis), plateDepth])        
            cube([barLeftRightPadding, frictionerWidth, frictionerHeight]);
        }
    }
}
