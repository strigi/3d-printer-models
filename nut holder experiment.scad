union() {
    difference() {
        cube([30, 30, 15]);
        translate([15, 15, -2.5])
            cylinder(h=20, r=5, $fn=100);
        translate([15, 15, 10])
            cylinder(h=7.5, r=11, $fn=100);       
    }
    pins(r=1.75);
}

module pins(r) {
    tangent = 11 - r;
    padding = 1;
    translate([15 - (tangent - padding) , 15, 10])
        cylinder(h=4, r=r, $fn=100);
    translate([15 + (tangent - padding) , 15, 10])
        cylinder(h=4, r=r, $fn=100);
    translate([15, 15 - (tangent - padding) , 10])
        cylinder(h=4, r=r, $fn=100);
    translate([15 , 15 + (tangent - padding), 10])
        cylinder(h=4, r=r, $fn=100);
}


//nut();

module nut() {
        axleHeight = 15;
        axleRadius = 10 / 2;
        holeRadius = 3.6 / 2;
        diskHeight = 3.6;    
        diskRadius = 22 / 2;
        lowerAxleHeight = 10;
    
        holeRingRadius = diskRadius - holeRadius - 1;
    
        cylinder(r=axleRadius, h=axleHeight, $fn=100);
        
        difference() {
            translate([0, 0, 10])
                cylinder(r=diskRadius, h=diskHeight, $fn=100);
            for(angle = [0 : 90 : 270])
                rotate(a=angle, v=[0, 0, 1])
                    translate([holeRingRadius, 0, lowerAxleHeight -1])
                        cylinder(r=holeRadius, h=diskHeight + 2, $fn=100);
        }
}