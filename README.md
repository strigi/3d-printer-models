# Strigi Cube

## Cura Machine Settings

### Printer Settings

- X (Width): `310mm`
- Y (Depth): `310mm`
- Z (Height): `200mm`
- Build plate shape: `Rectangular`
- Origin at center: `true`
- Heated bed: `true`
- G-code flavor: `Marlin`

### Printhead Settings

- X min: `?`
- Y min: `?`
- X max: `?`
- Y max: `?`
- Gantry height: `?`
- Number of Extruders: `1`

- Start G-code:

```
M104 S{material_standby_temperature} 
M140 S{material_bed_temperature_layer_0}

G34 ;Home + Z-stepper auto-align
M300 P125 ;Beep sound for 1/8s

;Safely move to bed warming holding position and wait for bed temperature
G1 F1000 Z5 ;Move platform down 0.5cm (clear build plate)
G1 F3000 X0 Y0 ;Move nozzle to center
G1 F3000 X-170 Y0
G1 F1000 Z0
G1 F3000 Y-175
M300 P128 ;Beep sound for 1/8s
M190 S{material_bed_temperature_layer_0}

; Safely move to nozzle warming holding position and wait for nozzle temperature
G1 F3000 Y0
M109 S{material_print_temperature_layer_0}

; Move to center to get started
M300 P500 ;Beep sound for 1/2s
G1 F1000 Z5
G1 F10000 X0 Y0
G92 E0 ;Reset extruder position to zero
; -----------------------
; End Strigi Start G-code
; -----------------------
```

- End G-code:

```
; -----------------------
; Begin Strigi End G-code
; -----------------------
M104 S0 ;Turn off extruder heating
M140 S0 ;Turn off heated bed
M106 S0 ;Turn off cooling fan

; Move nozzle safely away from print
G10 ;Retract (prevent drooping on print)
G91 ;Set relative positioning
G1 F1000 Z20 ;Lower plate by 1cm to clear the printed parts
G90 ; Set absolute positioning
G11 ;Unretract (restore previous retract)
G1 F3000 X0 Y0
G1 F3000 X0 Y100

M84 X Y ;Disable XY steppers (but keep powering Z to prevent the bed from dropping down)
M300 P1000 ;Beep sound for 1s
; ---------------------
; End Strigi End G-code
; ---------------------
```

### Nozzle Settings

- Nozzle size: `0.4 mm`
- Compatible material diameter: `1.75 mm`
- Nozzle offset X: `? (todo)`
- Nozzle offset Y: `? (todo)`
- Extruder Start G-code: `(todo: relevant for multiple extruders?)`
- Extruder End G-code: `(todo: relevant for multiple extruders?)`

---

## Material Settings

### Real Filament PLA

#### Manufacturer Properties

- Material Type: `PLA`
- Density: `1.24 g/cm³`
- Diameter: `1.75 mm` (tolerance: `0.05 mm`)
- Filament Cost: `€ 25.00`
- Filament weight: `1000 g`
- Nozzle temperature range: `190-210 degC`
- Bed temperature range: `?` 
- URL: https://real-filament.com/3d-filament/pla

#### My Settings

- Default printing temperature: `210 degC` (vendor's recommendation)
- Default build plate temperature: `60 degC` (based on Vicat softening point of material)
- Retraction distance: `1 mm` (direct drive extruders don't normally need more retraction)
- Retraction speed: `50 mm/s` (increased from default 25mm/s without any impact in print quality)
- Stand by temperature: `175 degC` (this is a temperature that does not cause oozing of the nozzle)
- Fan speed: `100%` (default, for PLA fan should always blow on maximum except for first layer)

### AliExpress PETG

#### Manufacturer Properties

- Material type: `PETG`
- Density: `1.27 g/cm³`
- Diameter: `1.75 mm` (tolerance: `0.05 mm`)
- Filament Cost: `€ 20.82`
- Filament weight: `1000 g`
- Nozzle temperature range: `200-240 degC`
- Bed temperature range: `100-120 degC` 
- URL: https://nl.aliexpress.com/item/2042369676.html?spm=a2g0s.9042311.0.0.27424c4dIH6as6


#### My Settings

- Default printing temperature: `230 degC`
- Default build plate temperature: `70 degC`
- Retraction distance: `1 mm`
- Retraction speed: `60 mm/s`
- Stand by temperature: `160 degC`
- Fan speed: `20%` (high fan speeds cause warping, zero fan speed causes part to melt/soften under nozzle causing very poor print results)

---

## Notes

These notes document some mistakes/experiences I made/gained while working on/with my printer.

### PETG Temperature, Bridging, Supports

For PETG whatever I try, it has almost no capability to do bridging. I asked ChatGPT and with tuning it says acceptable bridging
can be expected to span 1 cm, up to 2 cm max. This leads me to conclude that whoever talks about bridging and tuning their profiles
well for PETG, the amount of bridging they talk about is not very high. It seems to be only usable for small (screw) holes and other
realatively small features.

The practicality of bridging is thus very limited with PETG. For example, I attempted to make a square hole of 18x18mm
to fit a wooden bar for some home-improvement project, which seems to be not an option with PETG, even after trying to adjust temperature, and playing with the bridging settings.

#### Bridging Experiment (2024-08)

Unfortunately I went down that rabit hole, and created my own parameterized bridging test in FreeCAD [](bridging-test.FCStd) and printed various sizes with various bridging and temperature settings.

I came to the conclusion that the print temperature is not so important. With experimentation with print temperatures 200, 210, 220, 230 all bridging on PETG were equally bad with the
exception of 200 which produced spaghetti (so that is definitely too low).

What does seem to matter much more is the cooling fan for PETG. I discovered that my setting of 20% is too low to produce good bridging at all.
When bumping this up to 50% the results are much better (acceptable even). To compensate with the worse layer adhesion I also bumped the print
temperature to 240, which seems to be fine.

Unfortunately elsewhere in this document I concluded that printing with high fan speeds for PETG produces a lot of warping, so this seems to be a conflicting goal.

* 20% fan speed:
  * Good to reduce/eliminate warping
  * Horrible bridging
* 50% fan speed:
  * Possibly worse warping again (to be tested but to be expected based on earlier experimentation)
  * Much better bridging (remember what I said above, that this is always relative since PETG does not bridge very well, but it did produce acceptable bridge quality for short bridges (tested up to `20mm`))

Perhaps this begs for a compromise somewhere between 20% and 50%, say 33% to give the best of both worlds? This needs to be tested further.

### Supports PETG

Instead of air-bridging, I tend to prefer to print supports for PETG because of it's general lack of bridging.
However, this is whole other problem, because PETG tends to either fuse into the supports (i.e. impossible to remove supports)
or not stick at all causing the filament to just come loose in skin-to-support layers and creep onto the nozzle.

After some experimentation, the temperature did not seem to matter (I ended up printing even at higher temps than before)
what is much more important to have removable supports is the cooling fan. Setting it too low just exhibit one of the two phenomenons described in previous paragraph, depending on
what skin-to-support Z-distance you use (`0.1mm`: fuses the skin with the supports, 0.2mm: does not stick and drags it along eventually creeping onto the nozzle).

The best compromise that gave supports that were A) okayish removable and B) good skin quality was at 0.2mm or 0.15mm Z-gap (0.15 works in CURA even if the layer height is `0.2mm`, it generates a thinner (less flow?) layer for the 0.05mm mismatch).
I'm still experimenting the details but probably to get acceptable skins and removable supports the proper settings are:

- Z-gap: 0.15 or 0.20 (0.1 is too low)
- Cooling fan: somewhere > 20% (20% is too low) and <= 50% (above 50% not tested due to concerns with warping).

Trying out now the 0.15 - 50% combination, but I may go back to 0.2 and or reduce cooling to 33% to compromise with the dangers of warping.

Using concentric support interface. Generally harder to separate top surface (=overhang) than bottom surface. Printing even at 240 degrees
does not matter, it's the Z-gap and the cooling fan that matters.

### Retraction PETG

#### Retraction distance

Previously my printer was incorrectly configured to use 6.5mm retraction distance. This seems to have been the default for CURA
(probably because Ultimaker uses a bowden drive). Direct drive extruders should have very small retraction distance (e.g. <= 1mm).
I have discovered that this was a problem when I was trying to print a small overhang with supports. The walls printed directly after comming
from a support had severe underextrusion (causing the part to break). Further investigation also revealed excessive stringing between the support
sections. This was caused because CURA tries to optimize by not retracting for travels between supports. However this excessive stringing caused
to nozzle to become empty, causing the bad layer adhesion on the first wall after printing a support. At first I was looking for the option
`Limit Support Retractions`, but after some further research, I discovered that the retraction distance was way too high for my direct drive
printer. After setting this from 6.5mm to 1mm, the problem disappeared. As to the option `Limit Support Retractions`, I leave it off for now, but
it is unclear if this would further improve the print. However, as there is no problem anymore now, and the retraction settings were obviously
serioously wrong, I will leave it as it is for now. Lesson learned: direct drive extruder must not have high retraction! It is my hope that
other prints with small details (which require supports) will now also print much better, since I did previously note that small support
structures caused a lot of underextrusion (often causing the support tower to fall), this may have been the culprit here as well.

#### Retraction speed

After some further experience printing miniature figures with tiny details, I noticed there is still some stringing
when small parts with overhang supports are printed. This leads me to turn off `Limit Support Retractions` because it causes stringing
between the supports, leaving too little extrusion for the very tiny parts of the print (i.e. arms, magic staff, ...). I have also
taken the opportunity to look into the stringing problem in more details and printed a few
[stringing tuning tests](https://www.thingiverse.com/thing:2080224), which as pointed out that the option `Z Hop When Retracted` causes
some stringing on small parts. `Retraction Speed` is by default set to `25mm/s`, which is also currently [my firmware](https://gitlab.com/Eothel/Custom-Marlin-3D-Printer-Firmware/blob/1.1.x/Marlin/Configuration.h)'s DEFAULT_MAXIMUM_FEEDRATE setting.

2024-08-18 I have experimented with faster retract speeds for PETG, and the recommended speeds on the internet are somewhere in the range of 30-40.

#### Combing

Not to be confused with `coasting`.

I have discovered the correct setting for combing with PETG! It can be set to `all`. This will keep most travel moves within
the inner part avoiding to cross walls. This effectively helps to reduce any external stringing caused by sticky nozzle ooze being dragged in strings when crossing a wall. 

However it is very important `Max Comb Distance With No Retract` is set to a low value (currently I have `5.0 mm`). Otherwise the combing moves across infill
without retracting, and this causes the primed filament in the nozzle to be dragged into strings over the internal infill. Although this is not a problem visibly as the
strings are inside the part, it does cause excess filament to start sticking to the nozzle, leading to blobs and zits later in the print,
and empties the primed filament in the nozzle over long travel moves, resulting in the next extrude to have almost no material to deposit.
Typically this leads to the first few mm of a extrude path to be severely under-extruded to the point of failure.

With this setting the problem seems to have effectively disappeared (default value `0.0 mm` does no retraction while combing ever.)


### Concentric top/bottom layers

I have discovered that the feature CURA has to print top and bottom layers as concentric rings produces much more beautiful top and bottom layers
when printing objects witch are circular. The option is `Top/bottom pattern: concentric`.

### Ironing

I have discovered that the `Ironing` option produces absolutely stunning top surfaces! The tradeof is that it does take more time.
My first experiment was using `Ironing pattern: concentric` ironing (for a small coin shape). Any other ironing settings I left untouched.

The top layer was so incredibly flat that I couldn't feel any ridges with my finger (still felt it with my teeth though).
Although there still were some small irregularities (shiny spots), it was comparable with an injection molded piece
(which sometimes has the same type of irregularities).

One warning though is that immediately following the ironing, I did suffer small clogged nozzle which caused filament grinding (and the rest of
the print to fail). It is unknown if this was related to the ironing but it does seem suspicious (especially since during ironing, extrusion is
reduced to 10%. Did this cause the filament to be inside the hotend too long, causing a small clogging?). More experimentation remains to be done.

2024-08-17 update I have since used the ironing a lot for PETG with excellent results. The surface finish varies from stunning to very good.
No more clogging has happened (using Cura 5.8). In fact it's so good I use it as a default unless I need to get fast results (the ironing does take a lot of time)

To speed things up, for some models the Cura option `Iron Only Highest Layer` can help to avoid invisible (inner) surfaces from being ironed, but it depends on the
model.

### Bed adhesion

Recently I've been printing some large parts. A new set of problems occurs here which is the problem of _warping_. When the material doesn't stick
very well to the build plate, warping causes the corners of the bottom layers to "curl up" due to shrinking a little while cooling down
(mostly on the sides with small surface to build plate). Parts with a high mass (high infill, thick walls, ...) have this problem more since there
is more warping stress.

This can be combatted by assuring very strong bed adhesion (so the warping forces can be overcome and the part cools down) and gradual cooling down.
Another way to fight warping seems to be to have the build plate be heated up sufficiently (but not too much). Previously I reduced the print
temperature of my heated plate to 50°C. However, since this experience, I've increased it back to 60°C (as this is _Vicat softening point_ of my PLA).
It remains to be seen if this reduces warping issues.

As to bed adhesion, I have been using the following options:

#### Kapton tape (Obsolete)

- (+) Long life
- (-) Produces glossy bottom layer (inconsistent with rest of print) clearly showing individual nozzle strokes
- (-) Hard to install/replace (difficult to align and get water out from underneath or it sticks to soon)
- (-) Seams show up in print, wide tape is relatively expensive
- (-) Doesn't stick very well for small surfaces. Easily knocked loose. (tested for PLA only)

To apply: need to spray water to prevent sticking when aligning and then scraping out the water bubbles stuck underneath.
This is similar to applying blurring plastic (what's that named in English?) to a window.

Kapton is bad for PETG.

#### Blue painter's tape (Obsolete)

- (+) Sticks extremely well! It can even be hard to remove. (PLA)
- (+) Bottom layer looks very beautiful matte, more consistent with rest of print and when first layer is done right almost hides nozzle strokes
- (-) Underside not sticky enough (when warping forces are high, pulls the tape from the build plate, often comes loose when trying to remove print)
- (-) Poor reusability (easily damaged when removing prints)
- (-) Seams show up in print

According to my experiences, this is the best option. One lesson learned is that it's not necessary to cover the whole build plate in tape.
Since the tape is often not reusable for only a couple of times, it's best to just tape off the area that is going to be printed on.
This consumes a lot less tape and is much more practical to maintain. I drawed gridlines on the aluminium using a permanent marker to
have a better idea of position (cells of 1cm²) which makes it much easier to put the tape in the right place (use CURA's gridlines as a reference).

To clean, isopropanol wipe works quite well to restore adhesive behaviour (if the tap hasn't worn out by then). Also wipe it the very first time to clean it from whatever residue is on the tape roll.

#### Glue stick (Obsolete)

My experiences with glue stick are currently not very positive. It's more messy than it seems to be (even though it cleans of with water),
but especially does not stick nearly as well as blue painter's tape. So after a while, I ended up reverting back to the tape.

Update 2023-10-31 I have concocted a liquid solution with the glue stick to be able to apply it with a brush. Mix water (not too much) and solve the gluestick in the water. It is then sucked into a syringe so I have fine control over how much and where to apply.
TODO: Results remain to be seen.

#### Glass (Obsolete)

Experimented with a glass (mirror plate). It's supposed to stick for PETG so well that it rips of glass shards. In my experience, however, it didn't stick _at all_ (I cleaned the plate using isopropanol). Maybe the glass is covered with some sort of film?
TODO; try to clean with soap/sandy spunge?

#### Hairspray / Hairspray gel (Very good)

As of 2024-07-03 I find this to be the best option. But don't spray it (causes a mess), instead apply it with a brush (empty spray can in some recepticle and keep it closed from the air).

I'm using the L'Oreal Studio Line go create gel spray (#5 and #6).

I think I may have gotten good results using other tweaks now such as lowering fan speed, and wider higher hotter first layer temperature, so it seems less critical to apply the hairspray before every print.

This is the winner with the Spring sheet, I only occasionally have to apply (do not spray but use a brush to apply!) I use the Studio Line gel spray.
Defaulting to this one and it has not been a problem since for a long time. I do not know if this also works well with other surfaces than the steel sheet.

I also suspect I have tuned the printer's first layer much better: nailed bed leveling, supporting now to use Z-alignment and a EEPROM stored bilinear bed levelling + z-compensation.
Print temperature should be high, first layer printed thicker (150% lines, xy not the Z), Z-height works well for 0.3mm and 0.2mm usually using 0.2mm. Also important: first layer should print SLOOOOW
and prevent oozing or overextrusion (for PETG I find myself often setting the flow rate lower than 100% to say 90% or higher). Lower values cause under-extrusion effects.

Occasionally clean with isopropanol. Having the printer enclosed also helps a lot to prevent dust which is generally bad for adhesion and requires more frequent cleaning.

#### Spring plate (Very good)

Update: 2023-10-31 I have invested in a steel spring plate with magnetic sticker, which is very nice to remove parts. However, the surface is really bad for both PLA and PETG. Prints don't stick at all or not very well.

For PLA, using gluestick on the surface helps but often barely enough to have the first layer stick, after that the parts are easily knocked off or warp. Increasing the temperature also helps but in my experience not very reliably so (similar to the glue stick).

I have now tested to tape the spring plate with blue tape (this time reinforcing the sticky side with _Bison_ glue spray, but the same problems occur as with other blue tape experiments: the sticky side is not enough to keep the part from ripping of the tape) on the bottom side. The part itself sticks on it very very well on the top side (both for PETG and PLA). 

TODO: I need to find a good way to have the spring plate covered with some adhesive that sticks so well that the warping does not happen. So far, I have not found a satisfiable solves-all solution for PLA and PETG for both knock-off and warp problems :( Solutions have been "ad-hoc".

Update 2024-07-02 I have now gained a lot more experience with bed adhesion. It turns out that there are a few factors involved:

- The bed must be hotter than you think (PETG: 80deg gave me better results)
- The first layer can be made wider (e.g. 150% or more) and e.g 0.3 mm high (0.2 also works most of the time now).
- Flow can be made higher for the first layer which makes the plastic creep better into the spring steel plate
- Clean with Isopropyl Alcohol
- By far the best bed adhesion is hairspray, but do not spray it on (causes a mess), but spray it into a bottle and paint it on using a brush. It seems I don't even have to do this every time again (I have since encapsulated my printer so there is likely a lot less dust)
- A super important discovery is that large parts warp up from the bed on the edges a lot if the cooling fan is set too high! This is much more important than the temperature of the bed! I had a lot of frustration with PETG, but once I discovered that it should really have as little fan as possible all warping disappeared completely, and the prints are now perfect. (using encapsulated printer). I have printed a while with zero fan which produces really strong parts, but for delicate edges and such it print poorly (very bad overhangs, and small features melt and sag away while printing). I have since used a low fan speed of 20%, which seems to be close to a good balance.
- Higher first layer temperature

Update 2024-08: I have since resolved this issue completely with what I think is a combination of good first layer settings and the best adhesive being hair gel (Studio Line).

See [here](#hairspray-2024-08).

The biggest problem of the not-sticking was I believe to be much too high cooling for PETG causing a lot of warping. Warping vs Bridging is a balancing act for the proper cooling fan and I believe the proper values
are somewhere between 20% and 50% depending on leaning more towards warping vs bridging. See [](#petg-temperature-bridging-supports)
